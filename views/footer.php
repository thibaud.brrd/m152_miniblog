<?php
// Projet: M152_MiniBlog
// Script: Vue footer.php
// Description: Pied de page HTML pour les pages de l'application
// Auteur: Briard Thibaud
// Version 1.0.0 PC 27.01.2021, version initial

?>

            <footer>
                <div class="card col-sm-12">
                    <div class="card-body text-center">
                        &copy; M152_MiniBlog 2021, créé par Briard Thibaud
                    </div>
                </div>
            </footer>
        </div> <!-- class="container" -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
        <script src="js/ajax.js"></script>
    </body>
</html>