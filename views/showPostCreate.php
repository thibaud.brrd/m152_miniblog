<?php
// Projet: M152_MiniBlog
// Script: Vue showPostCreate.php
// Description: Formulaire de création des posts (create)
// Auteur: Briard Thibaud
// Version 1.0.0 PC 22.03.2021, version initial
?>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <form enctype="multipart/form-data" id="form-post-create">
                    <div class="form-group">
                        <label for="comment">Commentaire : </label>
                        <textarea id="comment" name="comment" class="form-control" rows="4" placholder="..."><?php if (isset($comment)) {echo($comment);} ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="new_medias">Médias : </label>
                        <input type="file" id="new_medias" name="new_medias[]" class="form-control" multiple="true" accept="image/*,video/*,audio/*">
                    </div>
                </form>
                <div class="form-group" id="upload-progress" style="display:none;">
                    <div class="progress">
                        <div id="upload-progress-bar" class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-secondary" onclick="cancelPost()">Annuler</button>
                    <button class="btn btn-primary" onclick="createPost()">Valider</button>
                </div>
            </div>
        </div>
    </div>
</div>