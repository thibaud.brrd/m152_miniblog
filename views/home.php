<?php
// Projet: M152_MiniBlog
// Script: Vue home.php
// Description: Page d'accueil du site
// Auteur: Briard Thibaud
// Version 1.0.0 PC 27.01.2021, version initial
// Version 1.1.0 PC 10.02.2021, ajout de l'affichage des posts
// Version 1.1.1 PC 24.02.2021, transfert du code pour l'affichage des médias dans les méthodes du fichier displayToolbox.php
// Version 1.2.0 PC 17.03.2021, ajout du dropdown pour la modification et la suppression des posts et d'un footer affichant la date de création et de modification du post

$pageTitle = "Home";

include "models/post.php";
include "models/media.php";
include "displayToolbox.php";

$posts = getAllPostsOrderByDateDesc();

include "header.php";
?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <img class="card-img-top" src="./medias/blob.gif" alt="pixel blob">
                <div class="card-body">
                    <h5 class="card-title">Bob deau</h5>
                    <p class="card-text">c'est un petit blob mignon</p>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <h2>Welcome</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    foreach ($posts as $index => $post) {
?>
<div class="container p-2">
    <div class="card">
        <?= displayMedias($post['idPost']) ?>
        <div class="card-body">
            <div class="row d-flex justify-content-between">
                <p class="col-11 card-text"><?= $post['comment'] ?></p>
                <div class="col-1 dropright">
                    <button class="btn btn-outline-secondary" type="button" id="post<?= $post['idPost'] ?>_OptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span>
                            <img src="ressources/icons/three-bars.svg">
                        </span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <h6 class="dropdown-header">Modification</h6>
                        <form class="d-flex justify-content-around" action="post.php" method="GET" id="post<?= $post['idPost'] ?>_form">
                            <input name="idPost" type="hidden" value="<?= $post['idPost'] ?>" id="post<?= $post['idPost'] ?>_form_IdValue">
                            <div class="form-group">
                                <button class="btn btn-outline-primary" type="submit" name="submit" value="update" form="post<?= $post['idPost'] ?>_form">
                                    <span><img src="ressources/icons/pencil.svg" alt="update"></span>
                                </button>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-outline-danger" type="submit" name="submit" value="delete" form="post<?= $post['idPost'] ?>_form">
                                    <span><img src="ressources/icons/trash.svg" alt="delete"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            <p><small>Date de création : <?= $post['creationDate'] ?></small></p>
            <p><small>Date de Modification : <?= $post['modificationDate'] ?></small></p>
        </div>
    </div>
</div>
<?php 
    }

include "footer.php"
?>
