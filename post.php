<?php
// Projet: M152_MiniBlog
// Script: Contrôleur post.php
// Description: affiche la page de modification des posts (edit/delete)
// Auteur: Briard Thibaud
// Version 1.0.0 PC 27.01.2021, version initial

include 'views/showPost.php';