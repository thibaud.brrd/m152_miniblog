<?php
// Projet: M152_MiniBlog
// Script: Modèle media.php
// Description: contient les fonctions en lien avec la table media.
// Auteur: Thibaud Briard
// Version 1.0.0 PC 10.02.2021, version initial

require_once 'models/dbConnect.php';

/**
 * récupère les enregistrements de la table media qui correspondent à l'id de post donnée
 * @return array tableau contenant les enregistrements
 */
function getAllMediasFormPost($idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT `m`.`idMedia`, `m`.`nameMedia`, `m`.`typeMedia`, `m`.`creationDate`, `m`.`modificationDate`
        FROM `media` as m
        WHERE `m`.`idPost` = :idPost
        ORDER BY `m`.`typeMedia` DESC");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);

    return $query;
}

/**
 * récupère le nombre d'enregistrements de la table media qui correspondent à l'id de post donnée
 * @return int nombre d'entrées trouvées
 */
function getNumberOfMediaForPost($idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT count(*) as 'count'
        FROM `media` as m
        WHERE `m`.`idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0]['count'];

    return $query;
}

/**
 * récupère le nombre d'enregistrements de type video ou image de la table media qui correspondent à l'id de post donnée
 * @return int nombre d'entrées trouvées
 */
function getNumberOfImagesOrVideosForPost($idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT count(*) as 'count'
        FROM `media` as m
        WHERE `m`.`idPost` = :idPost
        AND (`m`.`typeMedia` LIKE 'video%' OR `m`.`typeMedia` LIKE 'image%')");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0]['count'];

    return $query;
}

/**
 * récupère l'enregistrements de la table post qui correspondent à l'id de post donnée
 * @return array tableau contenant les enregistrements
 */
function getMedia($idMedia)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT `m`.`idMedia`, `m`.`nameMedia`, `m`.`typeMedia`, `m`.`creationDate`, `m`.`modificationDate`
        FROM `media` as m
        WHERE `m`.`idMedia` = :idMedia");
    $query->bindParam('idMedia', $idMedia, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0];
    return $query;
}

/**
 * récupère le nombre d'enregistrements de la table media qui correspondent à l'id de media donnée (0 ou 1) et le transforme en boolean
 * @return bool true si l'enregistrement existe, false si ce n'est pas le cas
 */
function checkMedia($idMedia)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT DISTINCT COUNT(*) as 'count'
        FROM `media` as m
        WHERE `m`.`idMedia` = :idMedia");
    $query->bindParam('idMedia', $idMedia, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0];
    return $query['count'] == 1 ? true : false;
}

/**
 * ajoute une nouvelle entrée à la table media
 * @return int id du dernier media nouvellement stocker
 */
function createMedia($nameMedia, $typeMedia, $idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "INSERT INTO `media` (`nameMedia`, `typeMedia`, `idPost`)
        VALUES (:nameMedia, :typeMedia, :idPost)");
    $query->bindParam('nameMedia', $nameMedia, PDO::PARAM_STR, 100);
    $query->bindParam('typeMedia', $typeMedia, PDO::PARAM_STR, 10);
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    
    return $connexion->LastInsertID();
}

function deleteMedia($idMedia) {
    $connexion = connectDB();
    $query = $connexion->prepare(
        "DELETE FROM `media`
        WHERE `idMedia` = :idMedia");
    $query->bindParam('idMedia', $idMedia, PDO::PARAM_INT, 11);
    $query->execute();
}

function deleteAllMediasFromPost($idPost) {
    $connexion = connectDB();
    $query = $connexion->prepare(
        "DELETE FROM `media`
        WHERE `idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
}