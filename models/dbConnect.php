<?php
// Projet: M152_MiniBlog
// Script: dbConnect.php
// Description: ce script permet la connexion a la DB
// Auteur: Briard Thibaud
// Version 1.0.0 PC 10.02.2020, version initial /!\ NON SÉCURISÉ /!\ (mot de passe : vide)

define("DB_NAME", "m152_miniblog");
define("DB_USER", "root");
define("DB_PASSWORD", "");

function connectDB()
{
    static $myDb = null;
    if ($myDb === null) {
        try {
            $myDb = new PDO(
                "mysql:host=localhost;dbname=" . DB_NAME . ";charset=utf8",
                DB_USER,
                DB_PASSWORD,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false)
            );
        }
        catch (Exception $e) {
            die("Impossible de se connecter à la base ". $e->getMessage());    
        }
    }
    return $myDb;
}