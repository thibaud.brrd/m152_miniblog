<?php
// Projet: M152_MiniBlog
// Script: Modèle post.php
// Description: contient les fonctions en lien avec la table post.
// Auteur: Thibaud Briard
// Version 1.0.0 PC 10.02.2021, version initial

require_once 'models/dbConnect.php';

/**
 * récupère tous les enregistrements de la table post
 * @return array tableau contenant les enregistrements
 */
function getAllPosts()
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT `p`.`idPost`, `p`.`comment`, `p`.`creationDate`, `p`.`modificationDate`
        FROM `post` as p");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    return $query;
}

/**
 * récupère tous les enregistrements de la table post ordré du plus récent au plus vieux
 * @return array tableau contenant les enregistrements ordré
 */
function getAllPostsOrderByDateDesc()
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT `p`.`idPost`, `p`.`comment`, `p`.`creationDate`, `p`.`modificationDate`
        FROM `post` as p
        ORDER BY `p`.`creationDate` DESC");
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    return $query;
}

/**
 * récupère l'enregistrements de la table post qui correspondent à l'id de post donnée
 * @return array tableau contenant les enregistrements
 */
function getPost($idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT `p`.`idPost`, `p`.`comment`, `p`.`creationDate`, `p`.`modificationDate`
        FROM `post` as p
        WHERE `p`.`idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0];
    return $query;
}

/**
 * récupère le nombre d'enregistrements de la table post qui correspondent à l'id de post donnée (0 ou 1) et le transforme en boolean
 * @return bool true si l'enregistrement existe, false si ce n'est pas le cas
 */
function checkPost($idPost)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "SELECT DISTINCT COUNT(*) as 'count'
        FROM `post` as p
        WHERE `p`.`idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
    $query = $query->fetchAll(PDO::FETCH_ASSOC);
    $query = $query[0];
    return $query['count'] == 1 ? true : false;
}

/**
 * ajoute une nouvelle entrée à la table post
 * @return int id du dernier post nouvellement stocker
 */
function createPost($comment)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "INSERT INTO `post` (`comment`)
        VALUES (:comment)");
    $query->bindParam('comment', $comment, PDO::PARAM_STR, 300);
    $query->execute();
    
    return $connexion->LastInsertID();
}

function updatePostComment($idPost, $comment)
{
    $connexion = connectDB();
    $query = $connexion->prepare(
        "UPDATE `post` as p
        SET `p`.`comment` = :comment
        WHERE `p`.`idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->bindParam('comment', $comment, PDO::PARAM_STR, 300);
    $query->execute();
    
    return $connexion->LastInsertID();
}

function deletePost($idPost) {
    $connexion = connectDB();
    $query = $connexion->prepare(
        "DELETE FROM `post`
        WHERE `idPost` = :idPost");
    $query->bindParam('idPost', $idPost, PDO::PARAM_INT, 11);
    $query->execute();
}